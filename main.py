import requests
from bs4 import BeautifulSoup
import csv

def fetch_html(url):
    try:
        response = requests.get(url)
        response.raise_for_status()
        return response.text
    except requests.exceptions.RequestException as e:
        print(f"Error fetching HTML: {e}")
        return None

def extract_metadata(html):
    metadata = {
        'title': '',
        'description': '',
        'body': ''
    }

    if html:
        soup = BeautifulSoup(html, 'html.parser')
        metadata['title'] = soup.title.string if soup.title else ''
        description_tag = soup.find('meta', attrs={'name': 'description'})
        metadata['description'] = description_tag['content'] if description_tag else ''
        body = soup.find('body')
        metadata['body'] = body.get_text() if body else ''

    return metadata

def save_to_csv(url, metadata):
    with open('metadata.csv', mode='a', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow([url, metadata['title'], metadata['description'], metadata['body']])

def main(url):
    html = fetch_html(url)
    if html:
        metadata = extract_metadata(html)
        save_to_csv(url, metadata)
        print("Title:", metadata['title'])
        print("Description:", metadata['description'])
        print("Body:", metadata['body'])

if __name__ == "__main__":
    test_urls = [
        "http://www.amazon.com/Cuisinart-CPT-122-Compact-2-Slice-Toaster/dp/B009GQ034C/ref=sr_1_1?s=kitchen&ie=UTF8&qid=1431620315&sr=1-1&keywords=toaster",
        "http://blog.rei.com/camp/how-to-introduce-your-indoorsy-friend-to-the-outdoors/",
        "http://www.cnn.com/2013/06/10/politics/edward-snowden-profile/"
    ]

    with open('metadata.csv', mode='w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(['URL', 'Title', 'Description', 'Body'])

    for url in test_urls:
        print("Fetching metadata for URL:", url)
        main(url)
        print("\n")